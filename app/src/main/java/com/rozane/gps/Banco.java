package com.rozane.gps;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Ro on 09/12/2016.
 */

public class Banco extends SQLiteOpenHelper {

    private static final String BANCO_DADOS = "banco";
    private static final int VERSAO = 1;
    
    private Context context;

    public Banco(Context context) {


        super(context, BANCO_DADOS, null, VERSAO);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String[] sql = context.getString(R.string.criacao_banco).split(";");
        
        db.beginTransaction(); //Inicia um transação no banco. Banco de dados fica no modo exclusivo

        try {
            ExecutarComandosSQL(db, sql);
            db.setTransactionSuccessful();
        } catch (SQLException ex) {

        } finally {
            db.endTransaction();
        }

    }

    private void ExecutarComandosSQL(SQLiteDatabase db, String[] sql) {
        for (String s : sql)
            if (s.trim().length() > 0)
                db.execSQL(s);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
