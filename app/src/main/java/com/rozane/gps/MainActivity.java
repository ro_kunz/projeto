package com.rozane.gps;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.sip.SipAudioCall;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String latituteStr;
    String longitudeStr;
    private TextView txtPassos;
    TextView textView;
    private LocationManager locationManager;

    private Button btnCalcular;

    private List<Coordenadas> coordenadas;

    public float distancia = 0;

    private Banco banco;

    // http://maps.googleapis.com/maps/api/staticmap?size=400x400&sensor=true&path=color:0x0000ff|weight:5|40.737102,-73.990318|40.749825,-73.987963|40.752946,-73.987384|40.755823,-73.986397

    String urlBase = "http://maps.googleapis.com/maps/api" +
            "/staticmap?size=400x400&sensor=true" +
            "&path=color:0x0000ff|weight:5|";

    String urlParam = "";

    private WebView mapa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mapa = (WebView) findViewById(R.id.mapa);

        txtPassos = (TextView) findViewById(R.id.txtPassos);

        textView = (TextView)findViewById(R.id.textView);

        btnCalcular = (Button)findViewById(R.id.btnCalcular);


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calcular();

            }
        });

        banco = new Banco(this);

        SQLiteDatabase sqLite = banco.getReadableDatabase();
        Cursor cursor = sqLite.rawQuery("SELECT * FROM coordenada", null);



        coordenadas = new ArrayList<>();

        if (cursor.moveToFirst()){

            do {
                Coordenadas coord = new Coordenadas();

                coord.setDistancia(cursor.getDouble(cursor.getColumnIndex("distancia")));
                coord.setLatitude(cursor.getDouble(cursor.getColumnIndex("latitude")));
                coord.setLongitude(cursor.getDouble(cursor.getColumnIndex("longitude")));
                coordenadas.add(coord);

                urlParam += coord.getLatitude() + "," + coord.getLongitude() + "|";

                distancia += coord.getDistancia();

            }while (cursor.moveToNext());

            urlParam = urlParam.substring(0, urlParam.length() - 1);
        }


        mapa.loadUrl(urlBase + urlParam);





    }


    public void calcular(){

        double resultado = (distancia*100) / Double.parseDouble(txtPassos.getText().toString());
        textView.setText("Quantidade de Passos: "+ (int) resultado);

        SQLiteDatabase sqLite = banco.getReadableDatabase();
        //Cursor cursor = sqLite.rawQuery("SELECT latitude, longitude FROM coordenada", null);

        ContentValues cv = new ContentValues();

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        cv.put("data", df.format(Calendar.getInstance().getTime()));
        cv.put("qntd_passos", resultado);
        cv.put("distancia_total", distancia);


        long result = sqLite.insert("resultado",null,cv);
        if (result > 0) {

            sqLite.delete("coordenada","id>0", null);

            Toast.makeText(this, "OK " , Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Erro" ,Toast.LENGTH_SHORT).show();
        }

        btnCalcular.setText("Fechar");
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
