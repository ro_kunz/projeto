package com.rozane.gps;

/**
 * Created by Ro on 11/12/2016.
 */

public class Resultado {

    private int id;
    private String data;
    private int qtd_passos;
    private double distancia_total;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getQtd_passos() {
        return qtd_passos;
    }

    public void setQtd_passos(int qtd_passos) {
        this.qtd_passos = qtd_passos;
    }

    public double getDistancia_total() {
        return distancia_total;
    }

    public void setDistancia_total(double distancia_total) {
        this.distancia_total = distancia_total;
    }



    @Override
    public String toString(){

        return data + " quantidade de passos: " + qtd_passos;

    }
}
