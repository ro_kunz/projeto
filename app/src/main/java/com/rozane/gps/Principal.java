package com.rozane.gps;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Principal extends AppCompatActivity implements LocationListener {

    private Button btnStart;
    private Button btnStop;
    private Button btnShow;

    private LocationManager locationManager;

    private ListView lvDados;

    Banco banco;

    List<Resultado> lista_resultado;

    private Coordenadas ultimoPonto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        banco = new Banco(this);


        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Iniciar();

            }
        });


        btnStop = (Button) findViewById(R.id.btnStop);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Parar();
            }
        });


        btnShow = (Button) findViewById(R.id.btnShow);
        btnShow.setVisibility(View.GONE);
        btnShow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Exibir();
            }
        });


        lvDados = (ListView) findViewById(R.id.lvDados);

        listarResultados();

    }



    public void Iniciar() {

        if (locationManager != null) {
            return;
        }

        locationManager = (LocationManager)
                this.getSystemService(Context.LOCATION_SERVICE);

        long tempoAtualizacao = 10;
        float distancia = 0;

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, tempoAtualizacao, distancia, this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }


    }


    public void Parar() {

        if (locationManager != null) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(this);


            btnShow.setVisibility(View.VISIBLE);

        }


    }



    public void Exibir(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }




    @Override
    public void onLocationChanged(Location location) {

        Coordenadas coordenadas = new Coordenadas();

        coordenadas.setLatitude(location.getLatitude());
        coordenadas.setLongitude(location.getLongitude());
        coordenadas.setSpeed(location.getSpeed());

        if (ultimoPonto != null){

            Location ultimo = new Location("ultima"); //Só guarda o último p/ calcular a distância
            ultimo.setLatitude(ultimoPonto.getLatitude());
            ultimo.setLongitude(ultimoPonto.getLongitude());
            double distancia = location.distanceTo(ultimo);
            coordenadas.setDistancia(distancia);

        }

        ultimoPonto = coordenadas;

        ContentValues cv = new ContentValues();

        cv.put("latitude", coordenadas.getLatitude());
        cv.put("longitude", coordenadas.getLongitude());
        cv.put("speed", coordenadas.getSpeed());
        cv.put("distancia", coordenadas.getDistancia());


        SQLiteDatabase sqLite = banco.getWritableDatabase();

        long resultado = sqLite.insert("coordenada",null,cv);
        if (resultado > 0) {
            Toast.makeText(this, "OK " + resultado, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Erro" + resultado, Toast.LENGTH_SHORT).show();
        }


    }

    public void listarResultados(){

        SQLiteDatabase sqLite = banco.getReadableDatabase();
        Cursor cursor = sqLite.rawQuery("SELECT * FROM resultado ORDER BY id desc", null);



        lista_resultado = new ArrayList<>();

        if (cursor.moveToFirst()){

            do {
                Resultado resultado = new Resultado();



                resultado.setData(cursor.getString(cursor.getColumnIndex("data")));
                resultado.setDistancia_total(cursor.getDouble(cursor.getColumnIndex("distancia_total")));
                resultado.setQtd_passos(cursor.getInt(cursor.getColumnIndex("qntd_passos")));
                lista_resultado.add(resultado);

            }while (cursor.moveToNext());

            ArrayAdapter<Resultado> adapter = new ArrayAdapter<Resultado>(this,
                    android.R.layout.simple_list_item_1, lista_resultado);

            lvDados.setAdapter(adapter);


        }

        sqLite.close(); //Fecha o banco
    }





    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    protected void onResume() {
        listarResultados();
        super.onResume();
    }

}

